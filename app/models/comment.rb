class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :images, :foreign_key => :image_id

  validates_presence_of :user_id, :image_id, :comment_text


end
